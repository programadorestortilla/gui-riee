SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `RieeDB` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `RieeDB` ;

-- -----------------------------------------------------
-- Table `RieeDB`.`docentes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `RieeDB`.`docentes` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `numControl` VARCHAR(20) NOT NULL,
  `nombre` VARCHAR(45) NOT NULL,
  `apellidoPaterno` VARCHAR(20) NULL,
  `apellidoMaterno` VARCHAR(20) NULL,
  `titulo` VARCHAR(25) NULL,
  `sexo` VARCHAR(1) NULL,
  `telefono` INT(10) NULL,
  `direccion` VARCHAR(85) NULL,
  `foto` VARCHAR(255) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `RieeDB`.`grupos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `RieeDB`.`grupos` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `idDocente` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_grupos_docentes1_idx` (`idDocente` ASC),
  CONSTRAINT `fk_grupos_docentes1`
    FOREIGN KEY (`idDocente`)
    REFERENCES `RieeDB`.`docentes` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `RieeDB`.`terapeutas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `RieeDB`.`terapeutas` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `numControl` VARCHAR(20) NOT NULL,
  `nombre` VARCHAR(45) NOT NULL,
  `apellidoPaterno` VARCHAR(20) NULL,
  `apellidoMaterno` VARCHAR(20) NULL,
  `titulo` VARCHAR(25) NULL,
  `sexo` VARCHAR(1) NULL,
  `telefono` INT(10) NULL,
  `direccion` VARCHAR(85) NULL,
  `foto` VARCHAR(255) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `RieeDB`.`alumnos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `RieeDB`.`alumnos` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `apellidoPaterno` VARCHAR(20) NOT NULL,
  `apellidoMaterno` VARCHAR(20) NOT NULL,
  `sexo` VARCHAR(1) NOT NULL,
  `edad` INT(2) NOT NULL,
  `nombreMadre` VARCHAR(85) NULL,
  `nombrePadre` VARCHAR(85) NULL,
  `nombreTutor` VARCHAR(85) NOT NULL,
  `direccion` VARCHAR(85) NULL,
  `telefono` INT(10) NULL,
  `fechaIngreso` TIMESTAMP NULL,
  `foto` VARCHAR(255) NULL,
  `infoExtra` TEXT NULL,
  `idDiscapacidad` INT NOT NULL,
  `idGrupo` INT NOT NULL,
  `idTerapeuta` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `idGrupo_idx` (`idGrupo` ASC),
  INDEX `fk_alumnos_terapeutas1_idx` (`idTerapeuta` ASC),
  CONSTRAINT `idGrupo`
    FOREIGN KEY (`idGrupo`)
    REFERENCES `RieeDB`.`grupos` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_alumnos_terapeutas1`
    FOREIGN KEY (`idTerapeuta`)
    REFERENCES `RieeDB`.`terapeutas` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `RieeDB`.`discapacidades`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `RieeDB`.`discapacidades` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `idActividad` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `idActividad_idx` (`idActividad` ASC),
  CONSTRAINT `idActividad`
    FOREIGN KEY (`idActividad`)
    REFERENCES `RieeDB`.`alumnos` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `RieeDB`.`actividades`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `RieeDB`.`actividades` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `descripcion` TEXT NOT NULL,
  `categorias` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `RieeDB`.`rutina`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `RieeDB`.`rutina` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `idActividad` VARCHAR(45) NULL,
  `fecha` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `RieeDB`.`estadisticas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `RieeDB`.`estadisticas` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `tiempoActividad` INT NOT NULL,
  `aciertos` INT NOT NULL,
  `errores` INT NOT NULL,
  `parametros` TEXT NOT NULL,
  `fecha` TIMESTAMP NOT NULL,
  `idRutina` INT NULL,
  `idAlumno` INT NULL,
  `idActividad` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_estadisticas_rutina1_idx` (`idRutina` ASC),
  INDEX `fk_estadisticas_alumnos1_idx` (`idAlumno` ASC),
  INDEX `fk_estadisticas_actividades1_idx` (`idActividad` ASC),
  CONSTRAINT `fk_estadisticas_rutina1`
    FOREIGN KEY (`idRutina`)
    REFERENCES `RieeDB`.`rutina` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_estadisticas_alumnos1`
    FOREIGN KEY (`idAlumno`)
    REFERENCES `RieeDB`.`alumnos` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_estadisticas_actividades1`
    FOREIGN KEY (`idActividad`)
    REFERENCES `RieeDB`.`actividades` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `RieeDB`.`admins`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `RieeDB`.`admins` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `pass` VARCHAR(25) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
