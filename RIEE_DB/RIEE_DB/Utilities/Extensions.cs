﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RIEE_DB.Utilities
{
    public static class Extensions
    {
        /// <summary>
        /// Returns a value indicating whether specified System.String object occurs within this string ignonring cases.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="value">The string to seek</param>
        /// <param name="comparison">One of the enumartion values that specifies the rules to search</param>
        /// <returns></returns>
        public static bool Contains(this string target, string value, StringComparison comparison)
        {
            return target.IndexOf(value, comparison) >= 0;
        }
    }
}
