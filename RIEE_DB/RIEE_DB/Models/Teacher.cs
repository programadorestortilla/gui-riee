﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RIEE_DB.Models
{
    class Teacher
    {
        public int ID { get; set; }
        public string Nickname { get; set; }
        public string Name {get; set;}
        public string Password { get; set; }

        public Teacher() { }

        public Teacher(string nickname, string password) {
            this.Nickname = nickname;
            this.Password = password;
        }

        public Teacher(string name, string password, int id) {
            this.Name = name;
            this.Password = password;
            this.ID = id;
        }
    }
}
