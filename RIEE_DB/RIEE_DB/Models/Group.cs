﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RIEE_DB.Models {

    class Group {

        private int _ID;
        private string _Name;
        private int _IdTeacher;

        public int ID {
            get { return _ID; }
            set { _ID = value; }
        }

        public string Name {
            get { return _Name; }
            set { _Name = value; }
        }

        public int IdTeacher {
            get { return _IdTeacher; }
            set { _IdTeacher = value; }
        }

        public Group() { }

        public Group(int Id, string name, int IdTeacher) {
            this._ID = Id;
            this._Name = name;
            this._IdTeacher = IdTeacher;
        }        
    }
}
