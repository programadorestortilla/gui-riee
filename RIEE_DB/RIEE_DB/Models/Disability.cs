﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RIEE_DB.Models {
    class Disability {
        #region Getters and Setters
        public int Id { get; set; }

        public string Name { get; set; }

        public int IdActivity { get; set; }

        #endregion

        public Disability(int id, string name, int idActivity) {
            this.Id = id;
            this.Name = name;
            this.IdActivity = idActivity;
        }

    }
}
