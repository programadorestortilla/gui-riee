﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace RIEE_DB.Models
{

    class Student : IDataErrorInfo{

        #region Getters and Setters
        public int Id { get; set; }

        [DisplayName("Nombre")]
        public string Name { get; set; }

        [DisplayName("Apellido Paterno")]
        public string FirstName { get; set; }

        [DisplayName("Apellido Materno")]
        public string LastName { get; set; }

        [DisplayName("Sexo")]
        public string Gender { get; set; }

        [DisplayName("Edad")]
        public int Age { get; set; }

        [DisplayName("Madre")]
        public string MotherName { get; set; }

        [DisplayName("Padre")]
        public string FatherName { get; set; }

        [DisplayName("Tutor")]
        public string TutorName { get; set; }

        [DisplayName("Domicilio")]
        public string Address { get; set; }

        [DisplayName("Numero de Telefono")]
        public int PhoneNumber { get; set; }

        [DisplayName("Fecha de Admisión")]
        public string DateOfAdmission { get; set; }

        public string Picture { get; set; }

        [DisplayName("Información Extra")]
        public string ExtraInfo { get; set; }

        public int IdDisability { get; set; }

        public int IdGroup { get; set; }

        public int IdTherapist { get; set; }
        #endregion

        #region Constructors
        public Student() { }

        public Student(string name, string firstName, string lastName, string gender, int age, string motherName, string fatherName, string tutorName, string address, int phoneNumber, string dateOfAdmission, string picture, string extraInfo, int idDisability, int idGroup, int idTherapist)
        {
            this.Name = name;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Gender = gender;
            this.Age = age;
            this.MotherName = motherName;
            this.FatherName = fatherName;
            this.Address = address;
            this.TutorName = tutorName;
            this.PhoneNumber = phoneNumber;
            this.DateOfAdmission = dateOfAdmission;
            this.Picture = picture;
            this.ExtraInfo = extraInfo;
            this.IdDisability = idDisability;
            this.IdGroup = idGroup;
            this.IdTherapist = idTherapist;
        }

        public Student(int id, string name, string firstName, string lastName, string gender, int age, string motherName, string fatherName, string tutorName, string address, int phoneNumber, string dateOfAdmission, string picture, string extraInfo, int idDisability, int idGroup, int idTherapist)
        {
            this.Id = id;
            this.Name = name;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Gender = gender;
            this.Age = age;
            this.MotherName = motherName;
            this.FatherName = fatherName;
            this.Address = address;
            this.TutorName = tutorName;
            this.PhoneNumber = phoneNumber;
            this.DateOfAdmission = dateOfAdmission;
            this.Picture = picture;
            this.ExtraInfo = extraInfo;
            this.IdDisability = idDisability;
            this.IdGroup = idGroup;
            this.IdTherapist = idTherapist;
        }
        #endregion

        #region IDataErrorInfo Members

        /// <summary>
        /// Don't use this method
        ///-only for XAML
        /// </summary>
        public string Error {
            get { throw new NotImplementedException(); }
        }

        public string this[string columnName] {
            get {
                string result = null;
                if (columnName == "Name") {
                    if (string.IsNullOrWhiteSpace(Name))
                        result = "Campo 'Nombre' no puede estar vacio y/o consistir de espacios en blanco";
                }
                if (columnName == "FirstName") {
                    if (string.IsNullOrWhiteSpace(FirstName))
                        result = "Apellido invalido";
                }
                if (columnName == "LastName") {
                    if (string.IsNullOrWhiteSpace(LastName))
                        result = "Apellido invalido";
                }
                if (columnName == "MotherName") {
                    if (string.IsNullOrWhiteSpace(MotherName))
                        result = "Nombre invalido";
                }
                if (columnName == "FatherName") {
                    if (string.IsNullOrWhiteSpace(FatherName))
                        result = "Nombre invalido";
                }
                if (columnName == "TutorName") {
                    if (string.IsNullOrWhiteSpace(TutorName))
                        result = "Nombre invalido";
                }
                if (columnName == "Address") {
                    if (string.IsNullOrWhiteSpace(Address))
                        result = "Campo domicilio no puede estar vacio";
                }
                if (columnName == "PhoneNumber") {
                    if (PhoneNumber == 0)
                        result = "Apellido invalido";
                }
                if (columnName == "FirstName") {
                    if (string.IsNullOrWhiteSpace(FirstName))
                        result = "Apellido invalido";
                }
                if (columnName == "Age") {
                    if (Age <= 0 || Age >= 99)
                        result = "Edad invalida";
                }

                return result;
            }
        }
        #endregion
    }
}
