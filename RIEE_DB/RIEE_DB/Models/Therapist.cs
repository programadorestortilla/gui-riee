﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using RIEE_DB.Utilities;

namespace RIEE_DB.Models {
    class Therapist:IDataErrorInfo
    {

        #region GETTERS & SETTERS
        public int ID { get; set; }

        [DisplayName("Numero de Control")]
        public string ControlNumber { get; set; }

        [DisplayName("Nombre")]
        public string Name { get; set; }

        [DisplayName("Apellido Paterno")]
        public string FirstName { get; set; }

        [DisplayName("Apellido Materno")]
        public string LastName { get; set; }

        [DisplayName("Titulo")]
        public string Degree { get; set; }

        [DisplayName("Sexo")]
        public string Gender { get; set; }

        [DisplayName("Numero de Telefono")]
        public int PhoneNumber { get; set; }

        [DisplayName("Direccion")]
        public string Address { get; set; }

        public string Picture { get; set; }

        #endregion

        #region CONSTRUCTORS

        public Therapist() { }

        public Therapist(string name, string firstName, string lastName,
            string degree, string gender, int phoneNumber, string address )
        {
            this.Name = name;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Degree = degree;
            this.Gender = gender;
            this.PhoneNumber = phoneNumber;
            this.Address = address;
            this.Picture = Picture;
        }

        #endregion


        #region IDATAERROR VALIDATORS
        public string Error{
            get{ throw new NotImplementedException();}
        }

        public string this[string columnName]
        {
            get{
                string result = null;

                if (columnName == "ControlNumber")
                {
                    if (string.IsNullOrWhiteSpace(ControlNumber))
                        result = "Numero de control inválido";
                }

                if(columnName == "Name")
                {
                    if(string.IsNullOrWhiteSpace(Name))
                        result = "Nombre invalido";
                }

                if(columnName == "FirstName")
                {
                    if(string.IsNullOrWhiteSpace(FirstName))
                        result="Apellido Invalido.";
                }

                if(columnName == "LastName")
                {
                    if(string.IsNullOrWhiteSpace(LastName))
                        result = "Apellido Invalido.";
                }

                if(columnName == "Degree")
                {
                    if(string.IsNullOrWhiteSpace(Degree))
                        result = "Titulo invalido.";
                }

                if(columnName == "PhoneNumber")
                {
                    if(PhoneNumber == 0)
                        result ="Numero de telefono invalido";
                }

                if(columnName == "Address")
                {
                    if(string.IsNullOrWhiteSpace(Address))
                        result ="Direccion invalida.";
                }

                if(columnName == "Gender")
                {
                    if (string.IsNullOrWhiteSpace(Gender))
                        result = "Género Inválido";
                    
                }

                return result;
            }
        }
        #endregion
    }
}
