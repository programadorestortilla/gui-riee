﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RIEE_DB.Models
{
    class Admin
    {
        private string name;
        private string psw;
        private int id;

        public string Name {
            get { return this.name; }
        }

        public string Password {
            get { return this.psw; }
        }
        /// <summary>
        /// create a new admin object empty
        /// </summary>
        public Admin() { }

        /// <summary>
        /// create an admin whith username and password
        /// </summary>
        /// <param name="name">username of the admin</param>
        /// <param name="password">password of the admin</param>
        public Admin(string name, string password) {
            this.name = name;
            this.psw = password;
        }

        public Admin(string name, string password, int id){
            this.name = name;
            this.psw = password;
            this.id = id;
        }
    }
}
