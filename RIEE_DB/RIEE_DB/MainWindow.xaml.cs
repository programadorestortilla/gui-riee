﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RIEE_DB.Models;
using RIEE_DB.Controllers;
using RIEE_DB.Views;

namespace RIEE_DB
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
       //Teacher teacher;
        Admin admin;
        DataBaseHandler db;

        /// <summary>
        /// Mainwindow
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            db = new DataBaseHandler();
        }

        private void login(object sender, RoutedEventArgs e)
        {
            //teacher = new Teacher(txtBoxName.Text,txtBoxPsw.Password);
            admin = new Admin(txtBoxName.Text, txtBoxPsw.Password);
            if (db.loginAsAdmin(admin))
            {
                MainPanel mainPanel = new MainPanel();
                mainPanel.Show();
                this.Close();
            }
            else {
                Alert.Visibility = Visibility.Visible;

            }
        }


        //Input Username Event
        string userLabel = "Usuario";
        private void txtBoxName_Loaded(object sender, RoutedEventArgs e)
        {
            txtBoxName.Text = userLabel;
            txtBoxName.Foreground = Brushes.Gray;
        }

        private void txtBoxName_GotFocus(object sender, RoutedEventArgs e)
        {
            if (txtBoxName.Text.Equals(userLabel))
            {
                txtBoxName.Text = string.Empty;
                txtBoxName.Foreground = Brushes.Black;
            }
        }

        private void txtBoxName_LostFocus(object sender, RoutedEventArgs e)
        {
            if(txtBoxName.Text.Equals(userLabel) || string.IsNullOrEmpty(txtBoxName.Text)){
                txtBoxName.Text = userLabel;
                txtBoxName.Foreground = Brushes.Gray;
            }
        }


        //Input Password Events
        string pswLabel = "Contraseña";
        private void txtBoxPsw_Loaded(object sender, RoutedEventArgs e)
        {
            txtBoxPsw.Password = pswLabel;
            txtBoxPsw.Foreground = Brushes.Gray;
        }

        private void txtBoxPsw_GotFocus(object sender, RoutedEventArgs e)
        {
            if(txtBoxPsw.Password.Equals(pswLabel)){
                txtBoxPsw.Password = string.Empty;
                txtBoxPsw.Foreground = Brushes.Black;
            }
        }

        private void txtBoxPsw_LostFocus(object sender, RoutedEventArgs e)
        {
            if (txtBoxPsw.Password.Equals(pswLabel) || string.IsNullOrEmpty(txtBoxPsw.Password))
            {
                txtBoxPsw.Password = pswLabel;
                txtBoxPsw.Foreground = Brushes.Gray;
            }
        }

    }
}
