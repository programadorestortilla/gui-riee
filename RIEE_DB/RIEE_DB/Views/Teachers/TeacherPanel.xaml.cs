﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RIEE_DB.Views.Teachers
{
    /// <summary>
    /// Interaction logic for TeacherPanel.xaml
    /// </summary>
    public partial class TeacherPanel : UserControl
    {
        private int _noOfErrorsOnScreen = 0;

        public TeacherPanel()
        {
            InitializeComponent();
        }

        private void Storyboard_Completed(object sender, EventArgs e)
        {
           // this.Content = new addTeacher();
        }

        private void validation_Error(object sender, ValidationErrorEventArgs e)
        {
            if (e.Action == ValidationErrorEventAction.Added)
                _noOfErrorsOnScreen++;
            else
                _noOfErrorsOnScreen--;
        }

        private void CommandBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {

        }

        private void CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void btnAddTeacher_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnCancelEdition_onClick(object sender, RoutedEventArgs e)
        {

        }

        private void cmbFilters_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void deleteTeacher(object sender, RoutedEventArgs e)
        {

        }

        private void editTeacher(object sender, RoutedEventArgs e)
        {

        }

        private void inputSearch_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void teachersTable_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void OnAutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {

        }

    }
}
