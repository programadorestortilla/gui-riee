﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RIEE_DB.Views.Teachers
{
    /// <summary>
    /// Interaction logic for AddTeacher.xaml
    /// </summary>
    public partial class AddTeacher : UserControl
    {
        public AddTeacher()
        {
            InitializeComponent();
        }

        private void btnBack_Event(object sender, RoutedEventArgs e)
        {

        }

        private void buttonImage_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
