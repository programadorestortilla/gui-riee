﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Animation;
using RIEE_DB.Controllers;
using RIEE_DB.Models;
using System.ComponentModel;

namespace RIEE_DB.Views.Students
{
    public partial class AddStudent : UserControl
    {

        private Storyboard slideAndFadeIn;
        private Storyboard slideAndFadeOut;
        private int _noOfErrorsOnScreen = 0;
        private Student _student = new Student();

        private string imageRoute = null;

        public AddStudent()
        {
            InitializeComponent();
            slideAndFadeIn = (Storyboard)Resources["SlideAndFadeIn"];
            slideAndFadeOut = (Storyboard)Resources["SlideAndFadeOut"];
            populateElements();

            grid.DataContext = _student;

            slideAndFadeIn.Begin();
        }

        private void backButton(object sender, RoutedEventArgs e)
        {
            slideAndFadeOut.Begin();
        }

        private void Storyboard_Completed(object sender, EventArgs e)
        {
            this.Content = new StudentPanel();
        }

        private void saveStudent()
        {
            string destination = @"..\..\Assets\Images\Profiles";
            if (!System.IO.Directory.Exists(destination))
                System.IO.Directory.CreateDirectory(destination);

            MessageBoxResult result = MessageBox.Show("Desea guardar el nuevo registro", "Guardar informacion", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                _student.Name = tbName.Text;
                _student.FirstName = tbFirstName.Text;
                _student.LastName = tbLastName.Text;
                _student.Gender = tbSex.Text;
                //Console.WriteLine(" [ADD_STUDENT.XAML.CS] gender : "+tbSex.Text);
                _student.Age = int.Parse(tbAge.Text);
                _student.MotherName = tbMother.Text;
                _student.FatherName = tbPhater.Text;
                _student.TutorName = tbTutor.Text;
                _student.Address = tbAddress.Text;
                _student.PhoneNumber = int.Parse(tbPhoneNumber.Text);
                _student.DateOfAdmission = datePicker.SelectedDate.Value.ToString();

                /*if (imageRoute != null)
                {
                    destination = System.IO.Path.Combine(destination, System.IO.Path.GetFileName(this.imageRoute));
                    System.IO.File.Copy(this.imageRoute, destination);
                    _student.Picture = destination;
                    Console.WriteLine("Destination: "+destination);
                }
                else
                    _student.Picture = "user_unknown.png";*/
                _student.Picture = "user_unknown.png";
                _student.ExtraInfo = textBoxInfo.Text;
                Disability d = comboBoxDisability.SelectedItem as Disability;
                _student.IdDisability = d.Id;
                Group g = comboBoxGroup.SelectedItem as Group;
                _student.IdGroup = g.ID;
                Therapist t = comboBoxTherapist.SelectedItem as Therapist;
                _student.IdTherapist = t.ID;
                using (var db = new DataBaseHandler())
                {
                    db.Student.insert(_student);
                }
                //back to another view
                slideAndFadeOut.Begin();
            }
        }

        private void comboBoxTherapist_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Therapist therapist = comboBoxTherapist.SelectedItem as Therapist;
            //MessageBox.Show("Seleccionad@ " + therapist.Name + " con ID " + therapist.ID);
        }

        private void populateElements()
        {

            using (var db = new DataBaseHandler())
            {
                // ---------------------------------------------------------------
                // Populate Therapist data
                // ---------------------------------------------------------------
                comboBoxTherapist.ItemsSource = db.Therapist.getAllTherapist();
                comboBoxTherapist.DisplayMemberPath = "Name";
                comboBoxTherapist.SelectedValuePath = "ID";
                comboBoxTherapist.SelectedIndex = 0;

                // ---------------------------------------------------------------
                // Populate Groups data
                // ---------------------------------------------------------------
                comboBoxGroup.ItemsSource = db.Group.getAllGroups();
                comboBoxGroup.DisplayMemberPath = "Name";
                comboBoxGroup.SelectedValuePath = "ID";
                comboBoxGroup.SelectedIndex = 0;

                // ---------------------------------------------------------------
                // Populate Disability data
                // ---------------------------------------------------------------
                comboBoxDisability.ItemsSource = db.Disability.getAllDisability();
                comboBoxDisability.DisplayMemberPath = "Name";
                comboBoxDisability.SelectedValuePath = "ID";
                comboBoxDisability.SelectedIndex = 0;

                // ---------------------------------------------------------------
                // Populate Teachers data
                // ---------------------------------------------------------------
                List<Teacher> teachersList = db.Teacher.getAllTeachers();
                comboBoxTeacher.ItemsSource = teachersList;
                comboBoxTeacher.DisplayMemberPath = "Name";
                comboBoxTeacher.SelectedValuePath = "ID";
                comboBoxTeacher.SelectedIndex = 0;

                // ---------------------------------------------------------------
                // Populate DatePicker with the actual date
                // ---------------------------------------------------------------
                datePicker.SelectedDate = DateTime.Today;
            }
        }

        private void validation_Error(object sender, ValidationErrorEventArgs e)
        {
            if (e.Action == ValidationErrorEventAction.Added)
                _noOfErrorsOnScreen++;
            else
                _noOfErrorsOnScreen--;
        }

        private void addStudent_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = _noOfErrorsOnScreen == 0;
            e.Handled = true;
        }

        private void addStudent_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Student std = grid.DataContext as Student;
            //write here code to add the student in the DB
            saveStudent();
            //reset UI
        }

        private void buttonImage_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".png";
            dlg.Filter = "JPEG Files (*.jpeg)|*.jpeg|PNG Files (*.png)|*.png|JPG Files (*.jpg)|*.jpg|GIF Files (*.gif)|*.gif";
            dlg.Title = "Selecciona una fotografia";
            // Display OpenFileDialog by calling ShowDialog method 
            Nullable<bool> result = dlg.ShowDialog();


            // Get the selected file name and display in a TextBox 
            if (result == true)
            {
                // Open document 
                this.imageRoute = dlg.FileName;
            }
            else
                this.imageRoute = null;
        }
    }
}
