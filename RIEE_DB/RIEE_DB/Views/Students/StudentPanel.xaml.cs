﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RIEE_DB.Models;
using RIEE_DB.Controllers;
using System.Windows.Media.Animation;
using System.ComponentModel;
using System.Reflection;
using RIEE_DB.Utilities;

namespace RIEE_DB.Views.Students
{
    public partial class StudentPanel : UserControl
    {
        private Storyboard slideAndFadeIn;
        private Storyboard slideAndFadeOut;
        private int _noOfErrorsOnScreen = 0;

        public StudentPanel()
        {
            InitializeComponent();
            slideAndFadeIn = (Storyboard)Resources["SlideAndFadeIn"];
            slideAndFadeOut = (Storyboard)Resources["SlideAndFadeOut"];
            fillTable();
            slideAndFadeIn.Begin();

           // MessageBoxResult result = MessageBox.Show("StudentPanel Instantiated. :D");
        }

        private void validation_Error(object sender, ValidationErrorEventArgs e)
        {
            if (e.Action == ValidationErrorEventAction.Added)
                _noOfErrorsOnScreen++;
            else
                _noOfErrorsOnScreen--;
        }

        private void addStudent_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = _noOfErrorsOnScreen == 0;
            e.Handled = true;
        }

        private void addStudent_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            //Student std = this.DataContext as Student;
            //write here code to add the student in the DB
            //populateInputs(std);
            saveStudent();
            //reset UI
        }


        public void fillTable()
        {
            // ---------------------------------------------------------------
            // Populate table
            // ---------------------------------------------------------------
            using (var db = new DataBaseHandler())
            {
                var studentsData = db.Student.getAllStudents();
                studentsTable.ItemsSource = studentsData;

                //cmbBoxGender.Items.Add("Masculino");
                //cmbBoxGender.Items.Add("Femenino");

                // ---------------------------------------------------------------
                // Populate Therapist data
                // ---------------------------------------------------------------
                cmbBoxTherapist.ItemsSource = db.Therapist.getAllTherapist();
                cmbBoxTherapist.DisplayMemberPath = "Name";
                cmbBoxTherapist.SelectedValuePath = "ID";
                cmbBoxTherapist.SelectedIndex = 0;

                // ---------------------------------------------------------------
                // Populate Groups data
                // ---------------------------------------------------------------
                cmbBoxGroup.ItemsSource = db.Group.getAllGroups();
                cmbBoxGroup.DisplayMemberPath = "Name";
                cmbBoxGroup.SelectedValuePath = "ID";
                cmbBoxGroup.SelectedIndex = 0;

                // ---------------------------------------------------------------
                // Populate Disability data
                // ---------------------------------------------------------------
                cmbBoxDisability.ItemsSource = db.Disability.getAllDisability();
                cmbBoxDisability.DisplayMemberPath = "Name";
                cmbBoxDisability.SelectedValuePath = "ID";
                cmbBoxDisability.SelectedIndex = 0;

                // ---------------------------------------------------------------
                // Populate Teachers data
                // ---------------------------------------------------------------
                cmbBoxTeacher.ItemsSource = db.Teacher.getAllTeachers();
                cmbBoxTeacher.DisplayMemberPath = "Name";
                cmbBoxTeacher.SelectedValuePath = "ID";
                cmbBoxTeacher.SelectedIndex = 0;
            }
        }

        private void addStudent(object sender, RoutedEventArgs e)
        {
            slideAndFadeOut.Begin();
        }

        private void Storyboard_Completed(object sender, EventArgs e)
        {
            this.Content = new AddStudent();
        }

        private void editStudent(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Seguro que quiere editar el alumno :" + inputName.Text + " ?", "Editar Alumno", MessageBoxButton.YesNo, MessageBoxImage.Question);
            
            if (result == MessageBoxResult.Yes)
            {
                enableDisableInputs(true);
                Student s = studentsTable.SelectedItem as Student;
                this.LayoutRoot.DataContext = s;
            }
        }

        private void enableDisableInputs(bool editMode)
        {
            if (editMode)
            {
                groupBoxPersonalInformation.IsEnabled = true;
                groupBoxOtherInfo.IsEnabled = true;
                groupBoxSchoolInformation.IsEnabled = true;
                groupBoxMedicalInformation.IsEnabled = true;
                studentsTable.IsEnabled = false;
                /* btnSave.IsEnabled = true;
                 inputName.IsEnabled = true;
                 inputAge.IsEnabled = true;
                 inputMotherName.IsEnabled = true;
                 inputFatherName.IsEnabled = true;
                 inputTutor.IsEnabled = true;
                 cmbBoxDisability.IsEnabled = true;
                 cmbBoxTherapist.IsEnabled = true;
                 cmbBoxGroup.IsEnabled = true;
                 cmbBoxTeacher.IsEnabled = true;
                 //datePicker.IsEnabled = true;*/

                btnDelete.IsEnabled = false;
                btnStatistics.IsEnabled = false;
                btnEdit.IsEnabled = false;
                btnSave.IsEnabled = true;
                btnCancelEdition.IsEnabled = true;
            }
            else
            {
                groupBoxPersonalInformation.IsEnabled = false;
                groupBoxOtherInfo.IsEnabled = false;
                groupBoxSchoolInformation.IsEnabled = false;
                groupBoxMedicalInformation.IsEnabled = false;
                studentsTable.IsEnabled = true;
                /*
                btnSave.IsEnabled = false;
                inputName.IsEnabled = false;
                inputAge.IsEnabled = false;
                inputMotherName.IsEnabled = false;
                inputFatherName.IsEnabled = false;
                inputTutor.IsEnabled = false;
                cmbBoxDisability.IsEnabled = false;
                cmbBoxTherapist.IsEnabled = false;
                cmbBoxGroup.IsEnabled = false;
                cmbBoxTeacher.IsEnabled = false;
                //datePicker.IsEnabled = false;*/

                btnDelete.IsEnabled = true;
                btnStatistics.IsEnabled = true;
                btnEdit.IsEnabled = true;
                btnSave.IsEnabled = false;
                btnCancelEdition.IsEnabled = false;

            }
        }

        private void saveStudent()
        {
            MessageBoxResult result = MessageBox.Show("Se ha modificado el alumno : " + (studentsTable.SelectedItem as Student).Name + ", desea guardar cambios?", "Guardar cambios", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                enableDisableInputs(false);
                Student student = new Student();
                student.Id = (studentsTable.SelectedItem as Student).Id;
                student.Name = inputName.Text;
                student.FirstName = inputFirstName.Text;
                student.LastName = inputLastName.Text;
                student.MotherName = inputMotherName.Text;
                student.FatherName = inputFatherName.Text;
                student.TutorName = inputTutor.Text;
                student.Age = int.Parse(inputAge.Text);
                student.Address = inputAdress.Text;
                student.PhoneNumber = int.Parse(inputPhone.Text);
                //student.Gender = cmbBoxGender.Text;
                Disability d = cmbBoxDisability.SelectedItem as Disability;
                student.IdDisability = d.Id;
                Group g = cmbBoxGroup.SelectedItem as Group;
                student.IdGroup = g.ID;
                Therapist t = cmbBoxTherapist.SelectedItem as Therapist;
                student.IdTherapist = t.ID;

                using (var db = new DataBaseHandler())
                    db.Student.update(student);

                fillTable(); 
            }
        }

        private void deleteStudent(object sender, RoutedEventArgs e)
        {
            Student student = studentsTable.SelectedItem as Student;

            if (student == null)
                return;

            MessageBoxResult result = MessageBox.Show("Desea eliminar el alumno : "+student.Name+" ?", "Eliminar alumno", MessageBoxButton.YesNo, MessageBoxImage.Warning);

            if (result != MessageBoxResult.Yes)
                return;

            using (var db = new DataBaseHandler())
            {
                db.Student.delete(student.Id);
            }

            fillTable();
        }

        private void OnAutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            e.Column.Header = ((PropertyDescriptor)e.PropertyDescriptor).DisplayName;

            List<string> hidenColumns = new List<string> { "Id", "Gender", "Age", "MotherName", "FatherName", "TutorName", "Address", "PhoneNumber", "Picture", "IdDisability", "IdGroup", "IdTherapist", "Error" };
            //List<string> hidenColumns = new List<string> { "Id", "IdDisability", "IdGroup", "IdTherapist", "Error" };

            if (hidenColumns.Contains(e.PropertyName, StringComparer.OrdinalIgnoreCase))
            {
                e.Column.Visibility = System.Windows.Visibility.Hidden;
            }
        }

        private void studentsTable_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Student s = studentsTable.SelectedItem as Student;
            enableDisableInputs(false);
            if (s == null)
                return;
            populateInputs(s);
        }

        
        private void populateInputs(Student student)
        {
            inputName.Text = student.Name;
            inputFirstName.Text = student.FirstName;
            inputLastName.Text = student.LastName;
            inputAge.Text = "" + student.Age;
            inputMotherName.Text = student.MotherName;
            inputFatherName.Text = student.FatherName;
            inputTutor.Text = student.TutorName;
            inputAdress.Text = student.Address;
            inputPhone.Text = "" + student.PhoneNumber;
            //cmbBoxGender.SelectedValue = student.Gender;
            //cmbBoxGender.SelectedValue = student.Gender.Contains("masculino", StringComparison.InvariantCultureIgnoreCase) ? "Masculino" : "Femenino";
            //cmbBoxDisability.SelectedValue = student.IdDisability;
            cmbBoxTherapist.SelectedValue = student.IdTherapist;
            cmbBoxGroup.SelectedValue = student.IdGroup;

            
            /*string temp = student.Picture;
            //student.Picture, UriKind.Absolute
            Console.WriteLine("Uri Prefix: \n" + "pack://application:,,,/RIEE_DB;component/" + student.Picture);
            temp = temp.Replace(@"\", @"/");
            Console.WriteLine("new Prefix: \n" + "pack://application:,,,/RIEE_DB;component/" + temp);
*/
            //imageFrame.Source = new BitmapImage(new Uri(@"pack://application:,,,/" + Assembly.GetExecutingAssembly().GetName().Name + ";component/"+temp, UriKind.RelativeOrAbsolute));
            //cmbBoxTeacher.SelectedItem = student.
            /*datePicker*/
        }

        private bool isFiltering = false;
        private string filterBy = "";

        private void cmbFilters_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var comboBox = sender as ComboBox;
            string value;
            value = ((ComboBoxItem)comboBox.SelectedItem).Content.ToString();

            switch (value)
            {
                case "Mostrar Todos":
                    isFiltering = false;
                    break;
                case "Nombre":
                    isFiltering = true;
                    filterBy = value;
                    break;
                case "Discapacidad":
                    isFiltering = true;
                    filterBy = value;
                    break;
                case "Grupo":
                    isFiltering = true;
                    filterBy = value;
                    break;
            }
        }

        private void inputSearch_OnKeyUpEvent(object sender, KeyEventArgs e)
        {
            if (!isFiltering)
                return;

            List<Student> students;
            using (var db = new DataBaseHandler())
            {
                students = db.Student.getAllStudents();
            }

            if (inputSearch.Text.Length <= 0)
            {
                studentsTable.ItemsSource = students;
                return;
            }

            switch (this.filterBy)
            {
                case "Nombre":
                    students = students.FindAll(student => student.Name.Contains(inputSearch.Text, StringComparison.OrdinalIgnoreCase));
                    break;
                case "Grupo":

                    List<Group> groups;
                    using (var db = new DataBaseHandler())
                    {
                        groups = db.Group.getAllGroups();
                    }
                    groups = groups.FindAll(group => group.Name.Contains(inputSearch.Text, StringComparison.OrdinalIgnoreCase));

                    students = (from student in students
                                join groupp in groups
                                on student.IdGroup equals groupp.ID
                                select student).ToList(); 

                    //students = students.Where( student=> groups.Any(group => student.IdGroup == group.ID)).ToList();
                    //elements = elements.FindAll(student => groups.TrueForAll(group => student.IdGroup == group.ID));

                    break;
                case "Discapacidad":

                    List<Disability> disabilities;
                    using (var db = new DataBaseHandler())
                    {
                        disabilities = db.Disability.getAllDisability();
                    }

                    disabilities = disabilities.FindAll(disability => disability.Name.Contains(inputSearch.Text, StringComparison.OrdinalIgnoreCase));
                    students = (from student in students
                                join disability in disabilities
                                    on student.IdDisability equals disability.Id
                                select student).ToList();
                    break;

            }

            studentsTable.ItemsSource = students;
        }

        private void btnCancelEdition_onClick(object sender, RoutedEventArgs e)
        {
            this.LayoutRoot.DataContext = null;
            
            enableDisableInputs(false);
            var std = studentsTable.SelectedItem as Student;
            fillTable();
            populateInputs(std);
        }

    }
}
