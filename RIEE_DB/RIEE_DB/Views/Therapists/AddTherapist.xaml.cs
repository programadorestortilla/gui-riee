﻿using RIEE_DB.Controllers;
using RIEE_DB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RIEE_DB.Views.Therapists
{
    /// <summary>
    /// Interaction logic for AddTherapist.xaml
    /// </summary>
    public partial class AddTherapist : UserControl
    {
        private int _noOfErrorsOnScreen = 0;
        private Storyboard slideAndFadeIn;
        private Storyboard slideAndFadeOut;
        private Therapist therapist = new Therapist();
        private string imageRoute = null;


        public AddTherapist()
        {
            InitializeComponent();

            slideAndFadeIn = (Storyboard)Resources["SlideAndFadeIn"];
            slideAndFadeOut = (Storyboard)Resources["SlideAndFadeOut"];
            this.LayoutRoot.DataContext = therapist;
            this.slideAndFadeIn.Begin();
        }

        private void Storyboard_Completed(object sender, EventArgs e)
        {
            this.Content = new TherapistPanel();
        }

        private void validation_Error(object sender, ValidationErrorEventArgs e)
        {
            if (e.Action == ValidationErrorEventAction.Added)
                _noOfErrorsOnScreen++;
            else
                _noOfErrorsOnScreen--;
        }

        private void CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            saveStudent();
        }

        private void CommandBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = _noOfErrorsOnScreen == 0;
            e.Handled = true;
        }

        private void btnBack_Event(object sender, RoutedEventArgs e)
        {
            slideAndFadeOut.Begin();
        }

        private void saveStudent()
        {
            MessageBoxResult result = MessageBox.Show("Desea guardar el nuevo registro", "Guardar información", MessageBoxButton.YesNo, MessageBoxImage.Question);

            if(result == MessageBoxResult.Yes)
            {
                therapist.ControlNumber = inputControlNumber.Text;
                therapist.Name = inputName.Text;
                therapist.FirstName = inputFirstName.Text;
                therapist.LastName = inputLastName.Text;
                therapist.Degree = inputDegree.Text;
                therapist.Gender = comboBoxSex.Text;
                therapist.PhoneNumber = Int32.Parse(inputPhoneNumber.Text);
                therapist.Picture = "userUnknown.png";

                using( var db = new DataBaseHandler())
                {
                    db.Therapist.insert(therapist);
                }

            }

            slideAndFadeOut.Begin();
        }

        private void buttonImage_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".png";
            dlg.Filter = "JPEG Files (*.jpeg)|*.jpeg|PNG Files (*.png)|*.png|JPG Files (*.jpg)|*.jpg|GIF Files (*.gif)|*.gif";
            dlg.Title = "Selecciona una fotografia";
            // Display OpenFileDialog by calling ShowDialog method 
            Nullable<bool> result = dlg.ShowDialog();


            // Get the selected file name and display in a TextBox 
            if (result == true)
            {
                // Open document 
                this.imageRoute = dlg.FileName;
            }
            else
                this.imageRoute = null;
        }
    }
}
