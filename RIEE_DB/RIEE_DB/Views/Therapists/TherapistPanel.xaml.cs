﻿using RIEE_DB.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RIEE_DB.Models;
using System.ComponentModel;
using RIEE_DB.Utilities;

namespace RIEE_DB.Views.Therapists
{
    /// <summary>
    /// Interaction logic for TherapistPanel.xaml
    /// </summary>
    public partial class TherapistPanel : UserControl
    {

        private Storyboard slideAndFadeIn;
        private Storyboard slideAndFadeOut;
        private int _noOfErrorsOnScreen = 0;

        public TherapistPanel()
        {
            InitializeComponent();
            slideAndFadeIn = (Storyboard)Resources["SlideAndFadeIn"];
            slideAndFadeOut = (Storyboard)Resources["SlideAndFadeOut"];
            fillTable();
            slideAndFadeIn.Begin();
        }

        private void Storyboard_Completed(object sender, EventArgs e)
        {
             this.Content = new AddTherapist();
        }

        private bool isFiltering = false;
        private string filterBy = "";

        private void cmbFilters_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var comboBox = sender as ComboBox;
            string value;
            value = ((ComboBoxItem)comboBox.SelectedItem).Content.ToString();

            switch (value)
            {
                case "Mostrar Todos":
                    isFiltering = false;
                    break;
                case "Nombre":
                    isFiltering = true;
                    filterBy = value;
                    break;
                case "Titulo":
                    isFiltering = true;
                    filterBy = value;
                    break;
                case "NoControl":
                    isFiltering = true;
                    filterBy = value;
                    break;
            }
        }

        private void inputSearch_KeyUp(object sender, KeyEventArgs e)
        {

            if (!isFiltering)
                return;

            List<Therapist> therapists;

            using(var db = new DataBaseHandler())
            {
                therapists = db.Therapist.getAllTherapist();
            }

            if(this.inputSearch.Text.Length <= 0)
            {
                this.therapistTable.ItemsSource = therapists;
                return;
            }

            switch (this.filterBy)
            {
                case "Nombre":
                    therapists = therapists.FindAll(therapist => therapist.Name.Contains(inputSearch.Text, StringComparison.OrdinalIgnoreCase));
                    break;
                case "Titulo":
                    therapists = therapists.FindAll(therapist => therapist.Degree.Contains(inputSearch.Text, StringComparison.OrdinalIgnoreCase));
                    break;
                case "NoControl":
                    therapists = therapists.FindAll(therapist => therapist.ControlNumber.Contains(inputSearch.Text));
                    break;

            }

            this.therapistTable.ItemsSource = therapists;
        }

        private void btnAddTherapist_Click(object sender, RoutedEventArgs e)
        {
            slideAndFadeOut.Begin();
        }

        private void therapistTable_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Therapist t = therapistTable.SelectedItem as Therapist;
            enableDisableInputs(false);
            if (t == null)
                return;
            populateInputs(t);
            
        }

        private void OnAutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            e.Column.Header = ((PropertyDescriptor)e.PropertyDescriptor).DisplayName;

            List<string> hidenColumns = new List<string> { "ID", "Gender", "Address", "PhoneNumber","Picture" ,"Error" };

            if (hidenColumns.Contains(e.PropertyName, StringComparer.OrdinalIgnoreCase))
            {
                e.Column.Visibility = System.Windows.Visibility.Hidden;
            }
        }

        private void validation_Error(object sender, ValidationErrorEventArgs e)
        {
            if (e.Action == ValidationErrorEventAction.Added)
                _noOfErrorsOnScreen++;
            else
                _noOfErrorsOnScreen--;
        }

        private void addTherapist_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = _noOfErrorsOnScreen == 0;
            e.Handled = true;
        }

        private void addTherapist_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            saveTherapist();
        }

        private void fillTable()
        {

            using (var db = new DataBaseHandler())
            {
                var therapistData = db.Therapist.getAllTherapist();
                therapistTable.ItemsSource = therapistData;

            }
        }

        private void enableDisableInputs(bool editMode)
        {
            if(editMode)
            {
                groupBoxPersonalInformation.IsEnabled = true;
                btnDelete.IsEnabled = false;
                btnEdit.IsEnabled = false;
                btnCancelEdition.IsEnabled = true;
                btnSave.IsEnabled = true;
                therapistTable.IsEnabled = false;
            }
            else
            {
                groupBoxPersonalInformation.IsEnabled = false;
                btnDelete.IsEnabled = true;
                btnEdit.IsEnabled = true;
                btnCancelEdition.IsEnabled = false;
                therapistTable.IsEnabled = true;
                btnSave.IsEnabled = false;
            }
        }

        
        private void populateInputs(Therapist therapist)
        {
            this.inputControlNumber.Text = therapist.ControlNumber;
            this.inputName.Text = therapist.Name;
            this.inputFirstName.Text = therapist.FirstName;
            this.inputLastName.Text = therapist.LastName;
            this.inputDegree.Text = therapist.Degree;
            this.inputPhoneNumber.Text =therapist.PhoneNumber.ToString();
            this.inputAddress.Text = therapist.Address;
        }

        private void deleteTherapist(object sender, RoutedEventArgs e)
        {
            Therapist t = therapistTable.SelectedItem as Therapist;

            if (t == null)
                return;

            MessageBoxResult result = MessageBox.Show("Desea eliminar el alumno : " + t.Name + " ?", "Eliminar terapeuta", MessageBoxButton.YesNo, MessageBoxImage.Warning);

            if (result != MessageBoxResult.Yes)
                return;

            using (var db = new DataBaseHandler())
            {
                db.Therapist.delete(t.ID);
            }
        }

        private void saveTherapist()
        {
            MessageBoxResult result = MessageBox.Show("Se ha modificado el alumno : " + (therapistTable.SelectedItem as Therapist).Name + ", desea guardar cambios?", "Guardar cambios", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                enableDisableInputs(false);
                Therapist therapist = new Therapist();
                therapist.ID = (this.therapistTable.SelectedItem as Therapist).ID;
                therapist.ControlNumber = this.inputControlNumber.Text;
                therapist.Name = this.inputName.Text;
                therapist.FirstName = this.inputFirstName.Text;
                therapist.LastName = this.inputLastName.Text;
                therapist.Degree = this.inputDegree.Text;
                therapist.Address = this.inputAddress.Text;
                therapist.PhoneNumber = Int32.Parse(this.inputPhoneNumber.Text);

                using (var db = new DataBaseHandler())
                    db.Therapist.update(therapist);

                fillTable();
            }
        }

        private void editTherapist(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Seguro que quiere editar el alumno :" + inputName.Text + " ?", "Editar Terapeuta", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                enableDisableInputs(true);
                Therapist t = therapistTable.SelectedItem as Therapist;
                this.groupBoxPersonalInformation.DataContext = t;
            }
        }

        private void btnCancelEdition_onClick(object sender, RoutedEventArgs e)
        {
            this.groupBoxPersonalInformation.DataContext = null;

            enableDisableInputs(false);
            //var therapist = therapistTable.SelectedItem as Therapist;
            fillTable();
            //populateInputs(therapist);

        }
    }
}
