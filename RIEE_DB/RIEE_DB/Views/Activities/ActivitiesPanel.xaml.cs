﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RIEE_DB.Views.Activities
{
    /// <summary>
    /// Interaction logic for ActivitiesPanel.xaml
    /// </summary>
    public partial class ActivitiesPanel : UserControl
    {
        /// <summary>
        /// ActivitiesPanel
        /// </summary>
        public ActivitiesPanel()
        {
            InitializeComponent();
        }
    }
}
