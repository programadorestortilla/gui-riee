﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using RIEE_DB.Models;
using RIEE_DB.Controllers;

namespace RIEE_DB.Views
{
    /// <summary>
    /// Interaction logic for MainPanel.xaml
    /// </summary>
    public partial class MainPanel : Window
    {
        /// <summary>
        /// MainPanel
        /// </summary>
        public MainPanel()
        {
            InitializeComponent();
        }
    }
}
