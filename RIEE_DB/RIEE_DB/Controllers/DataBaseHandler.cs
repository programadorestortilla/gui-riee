﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using RIEE_DB.Models;
using System.Windows;
using RIEE_DB.Utilities;

namespace RIEE_DB.Controllers
{
    class DataBaseHandler:DisposableClass
    {

        public StudentDBHandler Student;
        public TeacherDBHandler Teacher;
        public TherapistDBHandler Therapist;
        public GroupsDBHandler Group;
        public DisabilityDBHandler Disability;

        private DBConnect mConnection;
        private SQLiteCommand command;

        public DataBaseHandler()
        {
            mConnection = new DBConnect();
            command = new SQLiteCommand();
            command.Connection = mConnection.getConnection;

            Student = new StudentDBHandler(mConnection);
            Teacher = new TeacherDBHandler(mConnection);
            Therapist = new TherapistDBHandler(mConnection);
            Group = new GroupsDBHandler(mConnection);
            Disability = new DisabilityDBHandler(mConnection);
        }        

        public bool loginAsAdmin(Admin admin){
            mConnection.openConnection();
            command.CommandText = "select * from admins where nombre = '" + admin.Name + "' and pass = '" + admin.Password + "'";
            SQLiteDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                return true;
            }
            mConnection.closeConnection();
            return false;
        }

        protected override void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    // free other managed objects that implement
                    // IDisposable only

                    mConnection.Dispose();
                    command.Dispose();
                }

                // release any unmanaged objects
                // set object references to null
                Student = null;
                Teacher = null;
                Therapist = null;
                Group = null;
                Disability = null;
                _disposed = true;
            }

            base.Dispose(disposing);
        }
    }
 
   
    // ------------------------------------------------------------------------------
    // Private Classes
    // ------------------------------------------------------------------------------

    #region Students
    class StudentDBHandler
    {

        private DBConnect mConnection;
        private SQLiteCommand command;

        public StudentDBHandler(DBConnect conection)
        {
            mConnection = conection;
            command = new SQLiteCommand();
            command.Connection = mConnection.getConnection;
        }

        public void insert(Student student)
        {
            mConnection.openConnection();
            command.CommandText = "INSERT INTO alumnos(nombre,apellidoPaterno,apellidoMaterno,sexo,edad,nombreMadre,nombrePadre,nombreTutor,direccion,telefono,fechaIngreso,foto,infoExtra,idDiscapacidad,idGrupo,idTerapeuta)"
                                + "VALUES(@name, @firstName, @lastName, @gender, @age, @motherName, @fatherName, @tutorName, @address, @phoneNumber, @dateOfAdmission, @picture, @extraInfo, @idDisability, @idGroup, @idTherapist)";
            command.Parameters.AddWithValue("@name", student.Name);
            command.Parameters.AddWithValue("@firstName", student.FirstName);
            command.Parameters.AddWithValue("@lastName", student.LastName);
            command.Parameters.AddWithValue("@gender", student.Gender);
            command.Parameters.AddWithValue("@age", student.Age);
            command.Parameters.AddWithValue("@motherName", student.MotherName);
            command.Parameters.AddWithValue("@fatherName", student.FatherName);
            command.Parameters.AddWithValue("@tutorName", student.TutorName);
            command.Parameters.AddWithValue("@address", student.Address);
            command.Parameters.AddWithValue("@phoneNumber", student.PhoneNumber);
            command.Parameters.AddWithValue("@dateOfAdmission", student.DateOfAdmission);
            command.Parameters.AddWithValue("@picture", student.Picture);
            command.Parameters.AddWithValue("@extraInfo", student.ExtraInfo);
            command.Parameters.AddWithValue("@idDisability", student.IdDisability);
            command.Parameters.AddWithValue("@idGroup", student.IdGroup);
            command.Parameters.AddWithValue("@idTherapist", student.IdTherapist);
            command.ExecuteNonQuery();
            mConnection.closeConnection();
        }

        public void update(Student student)
        {
            mConnection.openConnection();
            command.CommandText = "UPDATE alumnos SET nombre = @nombre, " +
                                        "apellidoPaterno = @apellidoPaterno, " +
                                        "apellidoMaterno = @apellidoMaterno, " +
                                        "nombreMadre = @nombreMadre, " +
                                        "nombrePadre = @nombrePadre, " +
                                        "nombreTutor = @nombreTutor, " +
                                        "edad = @edad, " +
                                        "direccion = @direccion, " +
                                        "telefono = @telefono, " +
                                        "idDiscapacidad = @discapacidad, " +
                                        "idTerapeuta = @terapeuta, " +
                                        "idGrupo = @grupo " +
                                        "WHERE id = @idAlumno";
            //command.CommandText = "UPDATE alumnos SET nombre = @nombre WHERE id = @idAlumno";
            command.Parameters.AddWithValue("@nombre", student.Name);
            command.Parameters.AddWithValue("@apellidoPaterno", student.FirstName);
            command.Parameters.AddWithValue("@apellidoMaterno", student.LastName);
            command.Parameters.AddWithValue("@nombreMadre", student.MotherName);
            command.Parameters.AddWithValue("@nombrePadre", student.FatherName);
            command.Parameters.AddWithValue("@nombreTutor", student.TutorName);
            command.Parameters.AddWithValue("@edad", student.Age);
            command.Parameters.AddWithValue("@direccion", student.Address);
            command.Parameters.AddWithValue("@telefono", student.PhoneNumber);
            command.Parameters.AddWithValue("@sexo", student.Gender);
            command.Parameters.AddWithValue("@terapeuta", student.IdTherapist);
            command.Parameters.AddWithValue("@grupo", student.IdGroup);
            command.Parameters.AddWithValue("@discapacidad", student.IdDisability);
            command.Parameters.AddWithValue("@idAlumno", student.Id);

            Console.WriteLine("Name : "+student.Name );
            Console.WriteLine("ID : "+ student.Id);
            command.ExecuteNonQuery();
            mConnection.closeConnection();
        }

        public void delete(int idStudent)
        {
            mConnection.openConnection();
            command.CommandText = "DELETE FROM alumnos WHERE id = " + "'" + idStudent + "'";
            command.ExecuteNonQuery();
            mConnection.closeConnection();
        }

        public List<Student> getAllStudents()
        {
            List<Student> studentsArray = new List<Student>();
            mConnection.openConnection();
            command.CommandText = "SELECT * FROM alumnos";
            SQLiteDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                studentsArray.Add(new Student(Convert.ToInt32(reader["id"]),
                                                 (string)reader["nombre"],
                                                 (string)reader["apellidoPaterno"],
                                                 (string)reader["apellidoMaterno"],
                                                 (string)reader["sexo"],
                                                 Convert.ToInt32(reader["edad"]),
                                                 (string)reader["nombreMadre"],
                                                 (string)reader["nombrePadre"],
                                                 (string)reader["nombreTutor"],
                                                 (string)reader["direccion"],
                                                 Convert.ToInt32(reader["telefono"]),
                                                 (string)reader["fechaIngreso"],
                                                 (string)reader["foto"],
                                                 (string)reader["infoExtra"],
                                                 Convert.ToInt32(reader["idDiscapacidad"]),
                                                 Convert.ToInt32(reader["idGrupo"]),
                                                 Convert.ToInt32(reader["idTerapeuta"])));
            }
            reader.Close();
            mConnection.closeConnection();
            return studentsArray;
        }
    }
    #endregion

    #region Teachers
    class TeacherDBHandler
    {

        private DBConnect mConnection;
        private SQLiteCommand command;

        public TeacherDBHandler(DBConnect connection)
        {
            mConnection = connection;
            command = new SQLiteCommand();
            command.Connection = mConnection.getConnection;
        }

        public bool login(Teacher teacher)
        {
            mConnection.openConnection();
            command.CommandText = "select * from teacher where Nickname = '" + teacher.Nickname + "' and Password = '" + teacher.Password + "'";
            SQLiteDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                return true;
            }
            mConnection.closeConnection();
            return false;
        }

        public void insert(Teacher teacher) { }

        public void update(Teacher teacher) { }

        public void delete(int id) { }

        public List<Teacher> getAllTeachers()
        {
            List<Teacher> teacherList = new List<Teacher>();
            mConnection.openConnection();
            command.CommandText = "SELECT * FROM docentes";
            SQLiteDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                teacherList.Add(new Teacher(
                    reader.GetString(2),
                    reader.GetString(5),
                    reader.GetInt32(0)));
            }
            command.Dispose();
            mConnection.closeConnection();
            return teacherList;
        }

        public void logout(Teacher teacher)
        {

        }
    }
    #endregion

    #region Therapist
    class TherapistDBHandler
    {

        private DBConnect mConnection;
        private SQLiteCommand command;

        public TherapistDBHandler(DBConnect connection)
        {
            mConnection = connection;
            command = new SQLiteCommand();
            command.Connection = mConnection.getConnection;
        }

        public void delete(int idTherapist)
        {
            try
            {
                mConnection.openConnection();
                command.CommandText = "DELETE FROM terapeutas WHERE id = " + "'" + idTherapist + "'";
                command.ExecuteNonQuery();
                mConnection.closeConnection();
            }
            catch(SQLiteException e)
            {
                MessageBoxResult result = MessageBox.Show(e.ToString(), "Therapist Handler Error", MessageBoxButton.OK, MessageBoxImage.Error);   
            }
        }

        public void insert(Therapist therapist)
        {
            try
            {
                mConnection.openConnection();
                command.CommandText = "INSERT INTO terapeutas(numControl, nombre, apellidoPaterno, apellidoMaterno, titulo, sexo, telefono, direccion, foto)"
                                        + "VALUES(@numControl, @name, @firstName, @lastName, @degree, @gender, @phoneNumber, @address, @picture)";
                command.Parameters.AddWithValue("@numControl", therapist.ControlNumber);
                command.Parameters.AddWithValue("@name", therapist.Name);
                command.Parameters.AddWithValue("@firstName", therapist.FirstName);
                command.Parameters.AddWithValue("@lastName", therapist.LastName);
                command.Parameters.AddWithValue("@degree", therapist.Degree);
                command.Parameters.AddWithValue("@gender", therapist.Gender);
                command.Parameters.AddWithValue("@phoneNumber", therapist.PhoneNumber);
                command.Parameters.AddWithValue("@address", therapist.Address);
                command.Parameters.AddWithValue("@picture", therapist.Picture);
                command.ExecuteNonQuery();
                mConnection.closeConnection();
            }
            catch(SQLiteException e)
            {
                MessageBoxResult result = MessageBox.Show(e.ToString(), "Therapist Handler Error", MessageBoxButton.OK, MessageBoxImage.Error);   
            }
        }

        public void update(Therapist therapist)
        {
            try
            {
                mConnection.openConnection();
                command.CommandText = "UPDATE terapeutas SET numControl = @numControl, " +
                                        "nombre = @name, " +
                                        "apellidoPaterno = @firstName, " +
                                        "apellidoMaterno = @lastName, " +
                                        "titulo = @degree, " +
                                        "telefono = @phoneNumber, " +
                                        "direccion = @address " +
                                        "WHERE id = @idTherapist";
                command.Parameters.AddWithValue("@numControl", therapist.ControlNumber);
                command.Parameters.AddWithValue("@name", therapist.Name);
                command.Parameters.AddWithValue("@firstName", therapist.FirstName);
                command.Parameters.AddWithValue("@lastName", therapist.LastName);
                command.Parameters.AddWithValue("@degree", therapist.Degree);
                command.Parameters.AddWithValue("@phoneNumber", therapist.PhoneNumber);
                command.Parameters.AddWithValue("@address", therapist.Address);
                command.Parameters.AddWithValue("@idTherapist", therapist.ID);
                command.ExecuteNonQuery();
                mConnection.closeConnection();
            }
            catch (SQLiteException e)
            {
                MessageBoxResult result = MessageBox.Show(e.ToString(), "Therapist Handler Error", MessageBoxButton.OK, MessageBoxImage.Error);   
            }
        }

        public List<Therapist> getAllTherapist()
        {
            List<Therapist> therapistList = new List<Therapist>();
            mConnection.openConnection();
            command.CommandText = "SELECT * FROM terapeutas";
            SQLiteDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                /*Therapist therapist = new Therapist();
                therapist.ID = reader.GetInt32(0);
                therapist.Name = reader.GetString(2);
                therapistList.Add(therapist);*/

                Therapist therapist = new Therapist();
                therapist.ID = Convert.ToInt32(reader["id"]);
                therapist.ControlNumber = (string)reader["numControl"];
                therapist.Name = (string)reader["nombre"];
                therapist.FirstName = (string)reader["apellidoPaterno"];
                therapist.LastName = (string)reader["apellidoMaterno"];
                therapist.Degree = (string)reader["titulo"];
                therapist.Gender = (string)reader["sexo"];
                therapist.PhoneNumber = Convert.ToInt32(reader["telefono"]);
                therapist.Address = (string)reader["direccion"];
                therapist.Picture = (string)reader["foto"];
                therapistList.Add(therapist);
            }
            reader.Close();
            mConnection.closeConnection();
            return therapistList;
        }
    }
    #endregion

    #region Groups
    class GroupsDBHandler
    {

        private DBConnect mConnection;
        private SQLiteCommand command;

        public GroupsDBHandler(DBConnect connection)
        {
            mConnection = connection;
            command = new SQLiteCommand();
            command.Connection = mConnection.getConnection;
        }

        public List<Group> getAllGroups()
        {
            List<Group> groupList = new List<Group>();
            mConnection.openConnection();
            command.CommandText = "SELECT * FROM grupos";
            SQLiteDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                groupList.Add(
                    new Group(
                    reader.GetInt32(0),
                    reader.GetString(1),
                    reader.GetInt32(2)));
            }

            reader.Close();
            mConnection.closeConnection();
            return groupList;
        }

        public Group getGroupById(int id)
        {
            mConnection.openConnection();
            command.CommandText = "SELECT * FROM grupos WHERE id='" + id + "'";
            SQLiteDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                mConnection.closeConnection();

                var group = new Group(reader.GetInt32(0),
                    reader.GetString(1),
                    reader.GetInt32(2));
                reader.Close();
                return group;
            }
            else
            {
                mConnection.closeConnection();
                reader.Close();
                return null;
            }
        }
    }
    #endregion

    #region Disability
    class DisabilityDBHandler
    {

        private DBConnect mConnection;
        private SQLiteCommand command;

        public DisabilityDBHandler(DBConnect connection)
        {
            mConnection = connection;
            command = new SQLiteCommand();
            command.Connection = mConnection.getConnection;
        }

        public List<Disability> getAllDisability()
        {
            List<Disability> disabilityList = new List<Disability>();
            mConnection.openConnection();
            command.CommandText = "SELECT * FROM discapacidades";
            SQLiteDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                disabilityList.Add(new Disability(
                    reader.GetInt32(0),
                    reader.GetString(1),
                    reader.GetInt32(2)));
            }
            reader.Close();
            mConnection.closeConnection();
            return disabilityList;
        }
    }
    #endregion
}
    
