﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SQLite;
using System.Windows;
using RIEE_DB.Utilities;

namespace RIEE_DB.Controllers
{
    class DBConnect:DisposableClass
    {
        private SQLiteConnection _connection;
        //private string file = @"Assets/DB/RIEE_DB.sqlite";
        private string file = @"../../Assets/DB/RIEE_DB.sqlite";
        public SQLiteConnection getConnection { get { return _connection; } }

        public DBConnect()
        {
            this._connection = new SQLiteConnection("Data Source= " + file + ";Version=3;");
        }

        public bool openConnection()
        {
            try
                {
                _connection.Open();
                return true;
                }
                catch (SQLiteException e)
                {
                    switch (e.ErrorCode)
                    {
                      case 0:
                      MessageBox.Show("No se puede conectar al servidor");
                      break;
                      case 1045:
                      MessageBox.Show("Nombre de usuario/contraseña incorrectos");
                      break;
                    }
                  return false;
                }
         }

        public bool closeConnection()
        {
            try{
                _connection.Close();
                    return true;
            }catch(SQLiteException e)
            {
                MessageBox.Show(e.Message);
                return false;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    // free other managed objects that implement
                    // IDisposable only

                    _connection.Dispose();
                }

                // release any unmanaged objects
                // set object references to null

                _disposed = true;
            }

            base.Dispose(disposing);
        }
           
    }
}
