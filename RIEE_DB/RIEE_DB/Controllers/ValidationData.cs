﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Text.RegularExpressions;

namespace RIEE_DB.Controllers {
    class ValidationData : ValidationRule {

        private string _pattern;
        private string _message;
        private Regex _regrex;
        

        public string pattern {
            get { return _pattern; }
            set {
                _pattern = ((string)value);
                _regrex = new Regex(_pattern);
            }
        }

        public string message {
            get { return _message; }
            set { 
                _message = ((string)value);
            }
        }

        

        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo) {
            if ( _regrex.IsMatch(((string) value)) ) {
                return new ValidationResult(true, null);    
            }else{
                return new ValidationResult(false, _message);                           
            }
        }
    }
}
