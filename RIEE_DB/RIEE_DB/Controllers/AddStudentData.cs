﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace RIEE_DB.Controllers {
    class AddStudentData{

        private int _age;
        private string _name;
        private string _mother;
        private string _father;
        private string _tutor;
        private string _disability;
        

        public int Age {
            get { return _age; }
            set { _age = value; }
        }

        public string Name {
            get { return _name; }
            set { _name = value; }
        }

        public string Mother {
            get { return _mother; }
            set { _mother = value; }
        }

        public string Father {
            get { return _father; }
            set { _father = value; }
        }

        public string Tutor {
            get { return _tutor; }
            set { _tutor = value; }
        }

        public string Disability {
            get { return _disability; }
            set { _disability = value; }
        }
       
    }
}
